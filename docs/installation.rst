============
Installation
============

Install with ``pip``::

    $ python3 -m pip install myqueue --user

.. note::

    Python 3.7 or later is required.

Enable bash tab-completion for future terminal sessions like this::

    $ mq completion >> ~/.profile

Subscribe here_ if you want to be notified of updates on PyPI_.

.. _here: https://libraries.io/pypi/myqueue
.. _PyPI: https://pypi.org/project/myqueue/
